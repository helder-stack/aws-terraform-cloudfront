data "template_file" "s3-public-policy" {
  template = file("policy.json")

  vars = {bucket_name = local.domain}
}

resource "aws_s3_bucket" "example_bucket" {
  bucket        = local.domain
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "name" {
  bucket = aws_s3_bucket.example_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_policy" "allow_with_public_policy"{
  bucket = aws_s3_bucket.example_bucket.id
  policy = data.template_file.s3-public-policy.rendered
}
