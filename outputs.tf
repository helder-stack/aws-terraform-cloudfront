output "bucket_id" {
    value = aws_s3_bucket.example_bucket.id
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.example_bucket.bucket_regional_domain_name
}