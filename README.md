# EXPORT YOUR AWS KEYS

export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""

# START TERRAFORM COMMANDS
terraform init
terraform plan
terraform apply
